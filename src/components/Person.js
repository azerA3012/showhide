import React from 'react'
const Person = (props) => {
    return ( 
        <div className="person">
        <p>I'm {props.name} and I am {props.age}</p>
        <input type="text" onChange={props.changed} value={props.name}/>
        <button onClick={props.deletePerson}>Delete</button>
        
    </div>
     );
}
 
export default Person;