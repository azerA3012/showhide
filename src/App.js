import React, { Component } from "react";
import Person from "./components/Person";
import "./main.css";
import Button from "./components/Button";

class App extends Component {
  state = {
    persons: [
      { id: 1, name: "Alish", age: 12 },
      { id: 2, name: "Alish1", age: 29 },
      { id: 3, name: "Alish2", age: 39 },
    ],
    showPerson: true,
  };

  showPersonHandler = () => {
    console.log("show person clicked..." + this.state.showPerson);
    this.setState({
      showPerson: !this.state.showPerson,
    });
  };

  changeNameHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex((p) => {
      return p.id === id;
    });
    console.log(personIndex);

    const person = this.state.persons[personIndex];
    person.name = event.target.value;
    const persons = this.state.persons;

    persons[personIndex] = person;

    this.setState({
      persons: persons,
    });
  };

  deletePerson = (index) => {
    console.log("deleted..." + index);
    const persons = [...this.state.persons];
    persons.splice(index, 1);
    this.setState({
      persons: persons,
    });
  };

  showInput = (index) => {
    console.log("deleted..." + index);
    const persons = [...this.state.persons];
    persons.splice(index, 1);
    this.setState({
      showInput: !this.state.showPerson
    })
  };

  render() {
    let person = null;
    if (this.state.showPerson) {
      person = (
        <div>
          <div className="btn-div d-flex justify-content-center my-3 ">
            {this.state.persons.map((person, index) => {
              return (
                <Button
                  deletePerson={() => this.deletePerson(index)}
                  key={person.id}
                  showInput={() => this.showInput(index)}
                />
              );
            })}
          </div>

          {this.state.persons.map((person, index) => {
            return (
              <Person
                name={person.name}
                age={person.age}
                key={person.id}
                changed={(event) => this.changeNameHandler(event, person.id)}
                deletePerson={() => this.deletePerson(index)}
                
              />
            );
          })}
        </div>
      );
    }
    return (
      <div className="app">
        {this.state.persons.length ? (
          <button onClick={this.showPersonHandler}>Show/False</button>
        ) : (
          <div>bos</div>
        )}
        {person}
      </div>
    );
  }
}

export default App;
